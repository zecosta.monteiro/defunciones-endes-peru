
# Análisis de los datos de mortalidad de La Encuesta Demográfica y de Salud Familiar (ENDES), Peru


## Introducción
Este repositorio presenta los scripts en R, los guias y los datos para estimación de indicadores demográficos desde los datos de defunciones en el hogar de  La Encuesta Demográfica y de Salud Familiar (ENDES) de Peru en los años de 2018 y 2019.

La iniciativa es fruto de los esfuerzos de la equipe de CELADE - Centro Latinoamericano y Caribeño de Demografía, la División de Población de la Comisión Económica para América Latina y el Caribe (CEPAL).

##  Estructura de los repositorios

El repositorio se organiza en tres repositorios:
* `datos`: contiene los datos de caracteristicas del hogar las ENDES 2018 y 2019 (Modulo 64);
* `R`: contiene los scripts con funciones en R para las análisis;
* `docs`: contiene los guías de lectura de los datos de las ENDES y de estimación de indicadores demograficos como la tabla de mortalidad a partir de los scripts en R.

### `datos`

Las informaciones de defunciones e de población estan en el modulo 64 de lo [repositorio publico de los datos de las ENDES](http://iinei.inei.gob.pe/microdatos/Consulta_por_Encuesta.asp). El primer passo descrito en el guia de extración es hacer el download de estas informaciones y poner en el repositorio de acuerdo con las especificaciones de lectura. El directorio `datos` presenta los subdirectorios de los años de la encuesta en que se tienen los datos de defunciones en el hogar (`2018` y `2019`) con los questionarios, fichas tecnicas y datos para cada año de encuesta para el modulo 64 y el directorio `datos_processados` con los datos leidos y estructurados de las ENDES.

### `R`

El directorio `R` contiene los siguientes scripts:

* `lectura_estructura_endes.R`: contiene las funcciones y las rotinas de estructuración de las basis de datos necessarias para la construcción de los indicadores de mortalidad;

* `processamiento_datos_mortalidad.R`: contiene las funcciones y rotinas para estimación de población exposta al evento (defunciones) y de las defunciones de acuerdo con el periodo de referencia deseado;

*  `estimaciones_tablas_mortalidad.R`: contiene las funciones y los scripts para estimación de las funciones de la tabla de mortalidad;

* `metodos_suavizacion.R`: contiene las funciones con diferentes métodos de suavización de los datos de mortalidad de la tabla de mortalidad


### `docs`
