Lectura de los datos de las ENDES 2018 y 2019 en R
================

# Ajuste de lo ambiente R y cargamiento de funciones

Antes de comenzar a leer los datos, es fundamental limpiar el ambiente
de programación R. Esta etapa es una recomendación de buena practica de
programación y se recomenda para otras analisis que los hagan en el
futuro.

``` r
## 1.  ajuste de la area de trabajo R
### 1.1 desabilita graficos 
graphics.off( )
### 1.2 deleta objetos
rm( list = ls() )
```

En seguida, cargamos las funciones de lectura de los datos de mortalidad
y población de las ENDES que se encuentram en el repositorio `R/`.

``` r
## 2. carga funciones de lectura de datos
source('R/lectura_estructura_endes.R') # funciones de lectura de las ENDES
```

Los paquetes utilizados para la lectura se cargarán en las funciones y
se instalarán automáticamente si es necesario por intermedio de el
*script* `lectura_estructura_endes.R`.

# Lectura de los datos

Con las funciones cargadas y definidas, ejecutamos la lectura de los
datos de mortalidad con la función `read_endes_deaths.R` y de población
con la función `read_endes_pop.R`. Para las dos funciones los *inputs*
son el camino de lo repositorio de los datos para el respectivo año
(`dir_endes`) y el factor de escala para el factor de ponderación
(`scale_factor`). Este último possui como valor *default* 1000000, el
mismo para las encuestas de 2018 y 2019.

## Datos de población

La función `read_endes_pop.R` produce como *output* los microdatos de
caracteristicas básicas de la población de las ENDES con las siguientes
variables:

  - `id` : identificación de la entrevista;
  - `year_intvw` : año de la entrevista;
  - `month_intvw`: mes de la entrevista;
  - `clus` : conglomerado;
  - `strata` : estrato;
  - `weight` : peso muestral ajustado por el factor de escala definido
    en la función (con valor *default* de 1000000 para las encuestas de
    2018 y 2019);
  - `sex` : sexo (m: hombres y f: mujeres);
  - `age` : edad.

<!-- end list -->

``` r
## 3. Lectura de los datos de poblacion 
### 3.1 2018
poblacion_endes_2018 <- read_endes_pop( dir_endes = 'datos/2018')
head( poblacion_endes_2018 )
```

    ##           id year_intvw month_intvw clus strata   weight sex age
    ## 1: 000100701       2018           6    1      3 0.127995   m  46
    ## 2: 000100701       2018           6    1      3 0.127995   f  42
    ## 3: 000100701       2018           6    1      3 0.127995   f  16
    ## 4: 000100701       2018           6    1      3 0.127995   m  13
    ## 5: 000100701       2018           6    1      3 0.127995   m   8
    ## 6: 000100701       2018           6    1      3 0.127995   m   0

``` r
### 3.2 2019
poblacion_endes_2019 <- read_endes_pop( dir_endes = 'datos/2019')
head( poblacion_endes_2019 )
```

    ##           id year_intvw month_intvw clus strata   weight sex age
    ## 1: 000100201       2019           6    1      3 0.142155   m  43
    ## 2: 000100201       2019           6    1      3 0.142155   f  33
    ## 3: 000100201       2019           6    1      3 0.142155   f  12
    ## 4: 000100201       2019           6    1      3 0.142155   m   0
    ## 5: 000102801       2019           6    1      3 0.142155   m  28
    ## 6: 000102801       2019           6    1      3 0.142155   f  36

## Datos de mortalidad

La función `read_endes_deaths.R` produce como *output* los microdatos de
los fallecimientos de la ENDES con las siguientes variables:

  - `id` : identificación de la entrevista;
  - `year_intvw` : año de la entrevista;
  - `month_intvw` : mes de la entrevista;
  - `clus` : conglomerado;
  - `strata` : estrato;
  - `weight` : peso muestral ajustado por el factor de escala definido
    en la función (con valor default de 1000000 para las encuestas de
    2018 y 2019);
  - `year_dth` : año del fallecimiento;
  - `month_dth`: mes del fallecimiento;
  - `sex` : sexo de lo(a) fallecido(a) (m: hombres y f: mujeres);
  - `age` : edad en que falleció.

<!-- end list -->

``` r
## 4. Lectura de los datos de defunciones
### 4.1 2018
defunciones_endes_2018 <- read_endes_deaths( dir_endes = 'datos/2018')
head( defunciones_endes_2018 )
```

    ##           id year_intvw month_intvw clus strata   weight year_dth month_dth sex
    ## 1: 000205501       2018           4    2      2 0.364276     2017        12   f
    ## 2: 000406501       2018           4    4      4 0.115899     2017         4   f
    ## 3: 000603101       2018           2    6      2 0.674349     2015         3   m
    ## 4: 000704901       2018           6    7      4 0.846683     2016         6   f
    ## 5: 000712701       2018           6    7      4 0.140545     2017         2   f
    ## 6: 000800701       2018           5    8      9 0.500881     2017         7   m
    ##    age
    ## 1:  87
    ## 2:  87
    ## 3:  38
    ## 4:  72
    ## 5:  57
    ## 6:  58

``` r
### 4.1 2019
defunciones_endes_2019 <- read_endes_deaths( dir_endes = 'datos/2019')
head( defunciones_endes_2019 )
```

    ##           id year_intvw month_intvw clus strata   weight year_dth month_dth sex
    ## 1: 000400601       2019           5    4      4 0.721453     2014         6   m
    ## 2: 000417401       2019           5    4      4 0.121547     2017        10   f
    ## 3: 000803701       2019           4    8      9 0.388127     2018        10   f
    ## 4: 000806301       2019           4    8      9 0.083223     2016         5   f
    ## 5: 000808701       2019           4    8      9 0.388127     2017        11   m
    ## 6: 000903401       2019           4    9      8 0.165359     2015         2   m
    ##    age
    ## 1:  86
    ## 2:  38
    ## 3:  15
    ## 4:  11
    ## 5:   1
    ## 6:  64

## Processamiento de los datos

Los datos de población y mortalidad se processan en la función
`struct_mortality_endes.R`. Los inputs de esta función son:

  - `date_start`: fecha de início de lo periodo de referencia para las
    estimaciones de población exposta y de defunciones en el periodo;
  - `date_end`: fecha final de lo periodo de referencia para las
    estimaciones de población exposta y de defunciones en el periodo;
  - `pop_dat`: datos de población, *output* de la función
    `read_endes_pop`;
  - `death_dat`: datos de defunciones, *output* de la función
    `read_endes_deaths`;
  - `set_abridged`: `TRUE` para estimar exposición y defunciones en el
    periodo en edades quinquenales y `FALSE` para hacerlo en edad
    simples;
  - `set_weight`: `TRUE` para estimar exposición y defunciones
    utilizando el factor de ponderación y `FALSE` para estimarlos sin
    este factor, utilizando solamente las contajes de la muestra.

Los *outputs* de la función son los datos agregados de defunciones y
exposición a muerte en lo periodo requisitado en el formato `data.table`
con las siguientes variables:

  - `period`: periodo de referencia para los calculos de defunciones y
    poblacion exposta;
  - `date_ref`: fecha de referencia (medio de el periodo definido por
    `date_start` y `date_end`);
  - `date_ref_dec`: fecha de referencia en formato decimal;
  - `year_start`: ano inicial de lo periodo de referencia;
  - `year_end`: ano final de lo periodo de referencia;
  - `time_span`: tiempo transcurrido en anos entre la fecha inicial y la
    fecha final;
  - `sex`: sexo (m: hombres y f: mujeres);
  - `age`: edad en formato de edad simples (0, 1, 2,…, 88, 89, 90+
    cuando abridged = F) o en formato de grupos quinquenales (0, 1, 5,
    10, …, 80, 85, 90+ cuando abridged = T);
  - `pop_date_ref`: poblacion en el la fecha de referencia;
  - `deaths`: numero de defunciones en lo periodo de referencia;
  - `expos`: exposicion a muerte en anos-personas, calculada a partir de
    las contribuiciones en años de cada persona viva en el medio del
    periodo de referencia especificado;
  - `mx`: tazas especificas de mortalidad calculadas por la equación
    `mx` = `deaths` / `expos`.

<!-- end list -->

``` r
# 5. Processamiento de los datos
## 5.1 Datos en edades quinquenales para el periodo 2013-2017 con los datos de la ENDES 2018
datos_q_2018 <- 
        struct_mortality_endes( date_start = '2013-01-01',
                                date_end   = '2017-12-31',
                                pop_dat    = poblacion_endes_2018,
                                death_dat  = defunciones_endes_2018,
                                abridged   = TRUE,
                                weighted   = TRUE )
head(datos_q_2018)
```

    ##       period   date_ref date_ref_dec year_start year_end time_span sex age
    ## 1: 2013-2017 2015-07-02       2015.5       2013     2017         5   f   0
    ## 2: 2013-2017 2015-07-02       2015.5       2013     2017         5   f   1
    ## 3: 2013-2017 2015-07-02       2015.5       2013     2017         5   f   5
    ## 4: 2013-2017 2015-07-02       2015.5       2013     2017         5   f  10
    ## 5: 2013-2017 2015-07-02       2015.5       2013     2017         5   f  15
    ## 6: 2013-2017 2015-07-02       2015.5       2013     2017         5   f  20
    ##    pop_date_ref deaths     expos       mx
    ## 1:     1014.175 21.910  5068.094 0.004323
    ## 2:     5091.566 13.630 25443.879 0.000536
    ## 3:     5950.445  7.701 29735.923 0.000259
    ## 4:     5560.666 10.131 27788.095 0.000365
    ## 5:     4550.344 23.247 22739.253 0.001022
    ## 6:     5081.288 12.729 25392.518 0.000501

``` r
## 5.2 Datos en edades quinquenales para el periodo 2014-2018 con los datos de la ENDES 2019
datos_q_2019 <- 
        struct_mortality_endes( date_start = '2014-01-01',
                                date_end   = '2018-12-31',
                                pop_dat    = poblacion_endes_2019,
                                death_dat  = defunciones_endes_2019,
                                abridged   = TRUE,
                                weighted   = TRUE )
head(datos_q_2019)
```

    ##       period   date_ref date_ref_dec year_start year_end time_span sex age
    ## 1: 2014-2018 2016-07-01       2016.5       2014     2018         5   f   0
    ## 2: 2014-2018 2016-07-01       2016.5       2014     2018         5   f   1
    ## 3: 2014-2018 2016-07-01       2016.5       2014     2018         5   f   5
    ## 4: 2014-2018 2016-07-01       2016.5       2014     2018         5   f  10
    ## 5: 2014-2018 2016-07-01       2016.5       2014     2018         5   f  15
    ## 6: 2014-2018 2016-07-01       2016.5       2014     2018         5   f  20
    ##    pop_date_ref deaths     expos       mx
    ## 1:      961.036 15.286  4802.548 0.003183
    ## 2:     4956.389 20.834 24768.365 0.000841
    ## 3:     5731.428  4.986 28641.436 0.000174
    ## 4:     5423.234  2.194 27101.313 0.000081
    ## 5:     4817.754 19.327 24075.570 0.000803
    ## 6:     4929.160 12.754 24632.295 0.000518

## Tablas de mortalidad

Con los datos processados en manos, es possible hacer diversas analisis
de mortalidad para los periodos. Vamos a presentar algunos ejemplos con
los datos leidos.

### Curvas de mortalidad

``` r
# 6. Plots de lo perfil etario de la mortalidad
require( ggplot2 )

# 6.1 junta las dos basis de datos
datos_plot <- 
  rbind( datos_q_2018, datos_q_2019 )

# 6.2 plota las curvas por sexo y periodo
datos_plot %>%
  .[, sex_lab := ifelse( sex == 'f', 'Mujeres', 'Hombres' ) ] %>% # crea labels para variable sexo
  ggplot( aes( x = age, y = mx, group = period ) ) +
  geom_line( aes( color = period ), size = 1.25 ) +
  scale_y_log10( name = 'log(mx)' ) +
  scale_x_continuous( name = 'Edad', breaks = seq( 0, 90, 10 ) ) +
  scale_color_manual( values = c( 'black', 'red' ), name = 'Periodo' ) +
  facet_wrap( ~ sex_lab, ncol = 2 ) +
  theme_bw() +
  theme( legend.position = 'top',
         legend.direction = 'horizontal',
         axis.text = element_text( color = 'black', size = 12 ) )
```

![](man2_lectura_microdatos_files/figure-gfm/step6-1.png)<!-- -->
